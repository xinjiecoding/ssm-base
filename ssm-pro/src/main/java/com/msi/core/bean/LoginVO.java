package com.msi.core.bean;

import java.io.Serializable;

/**
 * 登录实体类
 */
public class LoginVO implements Serializable {

    private static final long serialVersionUID = 161715404506192923L;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户代码
     */
    private String userCode;

    /**
     * 密码
     */
    private String password;

    /**
     * 验证码
     */
    private String verifyCode;


    private String uuid;

    public LoginVO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "LoginVO{" +
                "userName='" + userName + '\'' +
                ", userCode='" + userCode + '\'' +
                ", password='" + password + '\'' +
                ", verifyCode='" + verifyCode + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
