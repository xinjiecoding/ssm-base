package com.msi.controller;


import com.msi.core.bean.LoginVO;
import com.msi.entity.SysUser;
import com.msi.service.SysUserService;
import com.msi.utils.RSAUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@RequestMapping("/login")
@Controller
public class LoginController {


    @Autowired
    private SysUserService sysUserService;

    @GetMapping(value= "/login")
    public String login(HttpServletRequest request, Map<String, Object> map) {

        // 生产RSA公私钥 ，并向前端发送 公钥相关数据，将私钥保存至session（如果使用redis,可考虑存在redis）
        Map<String, Object> rsaPublicAndPrivateKeys = RSAUtil.getRSAPublicAndPrivateKeys(request);
        map.put("pubexponent", rsaPublicAndPrivateKeys.get("pubexponent"));//保存公钥指数
        map.put("pubmodules", rsaPublicAndPrivateKeys.get("pubmodules"));//保存公钥系数
        request.getSession().setAttribute("prik", rsaPublicAndPrivateKeys.get("prik"));

        return "pages/login";
    }

    @PostMapping(value = "/login.do", produces = "application/json;charset=UTF-8") /*, produces = "application/text;charset=UTF-8"*/
    public String login(LoginVO loginVO, HttpServletRequest request) {

        String userName = loginVO.getUserName();
        String password = loginVO.getPassword();

        if (StringUtils.isBlank(userName) || StringUtils.isBlank(password)) return "loginError";

        password = RSAUtil.getRSADecodeStr(request, password);


        //使用 shiro 登录验证
        //1 认证的核心组件：获取 Subject 对象
        Subject subject = SecurityUtils.getSubject();
        //2 将登陆表单封装成 token 对象
        UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
        try {
            //3 让 shiro 框架进行登录验证：
            subject.login(token);
        } catch (Exception e) {
            e.printStackTrace();
            return "loginError";
        }

        request.getSession().removeAttribute("prik");

        return "pages/admin/index";
    }

    @PostMapping("/register")
    public String register(@RequestBody SysUser user) {
        sysUserService.saveSysUser(user);
        return "pages/login";
    }

    @GetMapping("/getUserAll")
    public Map<String, Object> getUserAll() {
        Map<String, Object> result = new HashMap<String, Object>();

        List<SysUser> sysUsers = sysUserService.getUserAll();
        result.put("data", sysUsers);

        return result;
    }

}
