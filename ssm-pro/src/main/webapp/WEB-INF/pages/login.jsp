<%@ page contentType="text/html;charset=UTF-8"  language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript" src="${ctx}/static/js/jquery/jquery-3.6.0.js"></script>
<script type="text/javascript" src="${ctx}/static/js/login/security.js"></script>
<head>
    <title>Title</title>
</head>
<body>
    <div>
        <form id="form-login" method="post" action="${ctx}/login.do">
            <input name="userName" id="userName"><br><br>
            <input name="password" id="password" type="password"><br><br>
            <button type="submit" style="height: 25px;width: 66px;" onclick="doLogin()">登录</button>
        </form>
    </div>
</body>

<script>

    function doLogin() {
        // debugger
        console.log('doLogin')
        RSAUtils.setMaxDigits(200);
        var key = new RSAUtils.getKeyPair("${pubexponent}", "", "${pubmodules}");
        var encrypedPwd = RSAUtils.encryptedString(key,$('#password').val());

        document.getElementById("password").value = encrypedPwd;

        $("#form-login").submit();
    }
</script>
</html>
